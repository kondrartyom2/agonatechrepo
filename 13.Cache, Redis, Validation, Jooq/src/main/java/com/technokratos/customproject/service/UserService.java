package com.technokratos.customproject.service;

import com.technokratos.customproject.dto.request.UserRequest;
import com.technokratos.customproject.dto.response.UserResponse;

import java.util.UUID;

public interface UserService {
    void createUser(UserRequest userRequest);

    UserResponse getUserById(UUID id);
}

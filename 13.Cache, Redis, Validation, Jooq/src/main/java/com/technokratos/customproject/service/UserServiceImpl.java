package com.technokratos.customproject.service;

import com.technokratos.customproject.dto.request.UserRequest;
import com.technokratos.customproject.dto.response.UserResponse;
import com.technokratos.customproject.model.jooq.schema.tables.pojos.AccountEntity;
import com.technokratos.customproject.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.UUID;

@RequiredArgsConstructor
@Service
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;

    @Override
    public void createUser(UserRequest userRequest) {
        userRepository.saveIfNotExist(
                AccountEntity.builder()
                        .createdAt(LocalDateTime.now())
                        .updatedAt(LocalDateTime.now())
                        .name(userRequest.getName())
                        .build()
        );
    }

    @Override
    public UserResponse getUserById(UUID id) {
        return userRepository.findUserById(id)
                .map(user -> UserResponse.builder()
                        .name(user.getName())
                        .build()
                ).orElseThrow(RuntimeException::new);
    }
}

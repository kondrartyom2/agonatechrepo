package com.technokratos.customproject.dto.response;

import com.technokratos.customproject.dto.validation.constraint.UserConstraint;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import java.time.Instant;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserResponse {

    private String name;

    private String login;

    private String password;

    private Instant birthday;
}

package com.technokratos.customproject.dto.validation.constraint;

import com.technokratos.customproject.dto.validation.validator.UserLoginAndNameValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

@Documented
@Constraint(validatedBy = {UserLoginAndNameValidator.class})
@Target({TYPE})
@Retention(RUNTIME)
public @interface UserConstraint {
    String message() default "{UserConstraint error}";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
package com.technokratos.customproject.dto.request;

import com.technokratos.customproject.dto.validation.constraint.UserConstraint;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.time.Instant;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
@UserConstraint
public class UserRequest {

    private String name;

    private String login;

    @NotBlank
    private String password;

    private Instant birthday;
}

package com.technokratos.customproject.dto.validation.validator;

import com.technokratos.customproject.dto.request.UserRequest;
import com.technokratos.customproject.dto.validation.constraint.UserConstraint;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Objects;

public class UserLoginAndNameValidator implements ConstraintValidator<UserConstraint, UserRequest> {
    private static final String MESSAGE = "User must have only login or name.";

    @Override
    public boolean isValid(UserRequest request, ConstraintValidatorContext context) {
        boolean valid = true;
        if (Objects.nonNull(request.getName()) & Objects.nonNull(request.getLogin())) {
            valid = false;
            buildConstraintViolationWithTemplate(context, "name");
        }

        if (Objects.isNull(request.getName()) & Objects.isNull(request.getLogin())) {
            valid = false;
            buildConstraintViolationWithTemplate(context, "name");
        }

        return valid;
    }

    private void buildConstraintViolationWithTemplate(ConstraintValidatorContext context, String fieldName) {
        context.buildConstraintViolationWithTemplate(MESSAGE)
                .addPropertyNode(fieldName)
                .addConstraintViolation()
                .disableDefaultConstraintViolation();
    }
}
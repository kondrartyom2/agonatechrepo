package com.technokratos.customproject;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

@SpringBootApplication
@EnableCaching
public class CustomProjectApplication {

    public static void main(String[] args) {
        SpringApplication.run(CustomProjectApplication.class, args);
    }

}

package com.technokratos.customproject.api;

import com.technokratos.customproject.dto.request.UserRequest;
import com.technokratos.customproject.dto.response.UserResponse;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.UUID;

@RequestMapping("/users")
public interface UserApi {

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    void createUser(@Valid @NotNull @RequestBody UserRequest request);

    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    UserResponse getUserById(@PathVariable UUID id);
}

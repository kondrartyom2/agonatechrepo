package com.technokratos.customproject.repository;

import com.technokratos.customproject.model.jooq.schema.tables.pojos.AccountEntity;

import java.util.Optional;
import java.util.UUID;

public interface UserRepository {
    void saveIfNotExist(AccountEntity entity);

    Optional<AccountEntity> findUserById(UUID id);
}

package com.technokratos.customproject.repository;

import com.technokratos.customproject.model.jooq.schema.tables.pojos.AccountEntity;
import lombok.RequiredArgsConstructor;
import org.jooq.DSLContext;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.UUID;

import static com.technokratos.customproject.model.jooq.schema.Tables.ACCOUNT_ENTITY;

@RequiredArgsConstructor
@Repository
public class UserRepositoryImpl implements UserRepository {

    private final DSLContext dsl;

    @Override
    public void saveIfNotExist(AccountEntity entity) {

        dsl.insertInto(ACCOUNT_ENTITY)
                .columns(ACCOUNT_ENTITY.CREATED_AT, ACCOUNT_ENTITY.UPDATED_AT, ACCOUNT_ENTITY.NAME)
                .values(entity.getCreatedAt(), entity.getUpdatedAt(), entity.getName())
                .execute();
    }

    @Override
    public Optional<AccountEntity> findUserById(UUID id) {
        return dsl.selectFrom(ACCOUNT_ENTITY)
                .where(ACCOUNT_ENTITY.ID.eq(id))
                .fetchOptionalInto(AccountEntity.class);
    }

}

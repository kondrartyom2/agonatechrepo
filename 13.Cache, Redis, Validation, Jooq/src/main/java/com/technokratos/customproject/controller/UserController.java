package com.technokratos.customproject.controller;

import com.technokratos.customproject.api.UserApi;
import com.technokratos.customproject.dto.request.UserRequest;
import com.technokratos.customproject.dto.response.UserResponse;
import com.technokratos.customproject.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

@RestController
@RequiredArgsConstructor
public class UserController implements UserApi {

    private final UserService userService;

    @Override
    public void createUser(UserRequest request) {
        userService.createUser(request);
    }

    @Cacheable(value = "users", key = "#id")
    @Override
    public UserResponse getUserById(UUID id) {
        return userService.getUserById(id);
    }
}

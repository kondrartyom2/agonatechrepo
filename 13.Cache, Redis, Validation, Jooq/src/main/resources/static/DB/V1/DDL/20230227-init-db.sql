create extension if not exists "uuid-ossp";

create table account(
    id uuid primary key default public.uuid_generate_v4(),
    name varchar not null,
    created_at timestamp without time zone not null default now(),
    updated_at timestamp without time zone
);

create table application(
                        id uuid primary key default public.uuid_generate_v4(),
                        name varchar not null,
                        created_at timestamp without time zone not null default now(),
                        updated_at timestamp without time zone,
                        user_id uuid not null,
                        foreign key(user_id) references account(id)
);


package com.technokratos.jdbc.repository;

import com.technokratos.jdbc.model.Account;

import java.util.List;
import java.util.UUID;

public interface AccountRepository extends CrudRepository<Account, UUID> {

    List<Account> findAllByFirstName(String firstName);
}

package com.technokratos.jdbc.repository;

import com.technokratos.config.ConnectionManager;
import com.technokratos.jdbc.model.Account;
import com.technokratos.jdbc.model.Device;
import com.technokratos.jdbc.model.Role;

import javax.swing.plaf.nimbus.State;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

public class DeviceRepositoryJdbcImpl implements CrudRepository<Device, Long> {

    public static final String SQL_FIND_ALL = "SELECT * FROM device";
    public static final String SQL_FIND_BY_ID = "SELECT * FROM device WHERE id = %s";
    public static final String SQL_INSERT = "INSERT into device(id, name) values ('%s', '%s')";

    private static final RowMapper<Device> DEVICE_ROW_MAPPER = row -> {
        Long id = row.getLong("id");
        String name = row.getString("name");
        return new Device(id, name);
    };

//    CREATE, UPDATE, DELETE, READ
//    CRUD

    private final Connection connection;

    public DeviceRepositoryJdbcImpl(Connection connection) {
        this.connection = connection;
    }

    public Long create(Device device) {
        try(Statement statement = connection.createStatement()) {
            Long id = Long.valueOf(UUID.randomUUID().toString());
            String sql = String.format(SQL_INSERT, id, device.getName());
            statement.execute(sql);
            return id;
        } catch (SQLException e) {
            throw new RuntimeException("Exception on device findAll", e);
        }
    }

    public Device update(Device device) {
        return device;
    }

    public void deleteById(Long id) {

    }

    public Optional<Device> findById(Long deviceId) {
        try(Statement statement = connection.createStatement()) {
            ResultSet resultSet = statement.executeQuery(String.format(SQL_FIND_BY_ID, deviceId));
            if (resultSet.next()) {
                return Optional.ofNullable(DEVICE_ROW_MAPPER.mapRow(resultSet));
            }
            return Optional.empty();
        } catch (SQLException e) {
            throw new RuntimeException("Exception on device findAll", e);
        }
    }

    public List<Device> findAll() {
        try(Statement statement = connection.createStatement()) {
            ResultSet resultSet = statement.executeQuery(SQL_FIND_ALL);
            List<Device> devices = new ArrayList<>();
            while (resultSet.next()) {
                Device device = DEVICE_ROW_MAPPER.mapRow(resultSet);
                devices.add(device);
            }
            return devices;
        } catch (SQLException e) {
            throw new RuntimeException("Exception on device findAll", e);
        }
    }
}

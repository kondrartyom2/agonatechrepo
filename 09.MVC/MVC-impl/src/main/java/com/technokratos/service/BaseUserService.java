package com.technokratos.service;

import com.technokratos.dto.request.User;
import com.technokratos.exception.UserNotFoundException;
import com.technokratos.model.UserEntity;
import com.technokratos.repository.UserRepository;
import com.technokratos.util.mapper.UserMapper;
import dto.request.UserRequest;
import dto.response.UserResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.List;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class BaseUserService implements UserService {

    private final UserRepository repository;
    private final UserMapper mapper;
    private final RestTemplate template;

    @Override
    public void createUser(UserRequest request) {

        Void response = template.postForObject(
                "http://localhost:8080/users",
                User.builder()
                        .email("kondrartyom2@gmail.com")
                        .accessed(true)
                        .name("artem")
                        .build(),
                Void.TYPE
        );
        repository.save(mapper.toEntity(request));
    }

    @Override
    public UserResponse updateUser(UUID userId, UserRequest request) {
        return null;
    }

    @Override
    public void deleteUserById(UUID userId) {
        repository.deleteById(userId);
    }

    @Override
    public UserResponse getUserById(UUID userId) {
        return repository.findById(userId)
                .map(mapper::toResponse)
                .orElseThrow(() -> new UserNotFoundException(userId));
    }

    @Override
    public List<UserResponse> getUsers() {
        return repository.findAll().stream().map(mapper::toResponse).toList();
    }
}

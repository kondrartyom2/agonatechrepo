package com.technokratos.databasestarter.config.properties;

import lombok.Data;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Data
@ConfigurationProperties(prefix = "technokratos.starters")
public class MyAwesomeProps {

    private String username;
    private String password;
}

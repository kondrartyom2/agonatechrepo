package com.technokratos.databasestarter.config;

import com.technokratos.databasestarter.config.properties.MyAwesomeProps;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import liquibase.integration.spring.SpringLiquibase;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.boot.autoconfigure.liquibase.LiquibaseProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.Database;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

@Configuration
@EnableJpaRepositories
@ConditionalOnProperty(prefix = "spring.datasource", value = "enabled", havingValue = "true", matchIfMissing = false)
public class JpaConfig {

    @Bean
    public MyAwesomeProps props() {
        return new MyAwesomeProps();
    }

    @Bean
    @ConditionalOnMissingBean(DataSourceProperties.class)
    public DataSourceProperties dataSourceProperties(){
        return new DataSourceProperties();
    }

    public DataSource dataSource(){
        return new HikariDataSource(hikariConfig());
    }

    @Bean
    @ConditionalOnMissingBean(HikariConfig.class)
    public HikariConfig hikariConfig(){

        DataSourceProperties properties = dataSourceProperties();

        HikariConfig config = new HikariConfig();


        config.setJdbcUrl(properties.getUrl());
        config.setUsername(properties.getUsername());
        config.setPassword(properties.getPassword());
        config.setDriverClassName(properties.getDriverClassName());
        return config;
    }

    @Bean
    @ConditionalOnMissingBean(LiquibaseProperties.class)
    public LiquibaseProperties liquibaseProperties(){
        return new LiquibaseProperties();
    }

    @Bean
    @ConditionalOnMissingBean(SpringLiquibase.class)
    public SpringLiquibase liquibase() {
        SpringLiquibase liquibase = new SpringLiquibase();
        liquibase.setChangeLog(liquibaseProperties().getChangeLog());
        liquibase.setDataSource(dataSource());
        return liquibase;
    }

    @Bean
    public EntityManagerFactory entityManagerFactory() {

        HibernateJpaVendorAdapter adapter = new HibernateJpaVendorAdapter();
        adapter.setDatabase(Database.POSTGRESQL);
        LocalContainerEntityManagerFactoryBean entityManagerFactoryBean
                = new LocalContainerEntityManagerFactoryBean();
        entityManagerFactoryBean.setDataSource(dataSource());
        entityManagerFactoryBean.setJpaVendorAdapter(adapter);
        return entityManagerFactoryBean.getObject();
    }

    @Bean
    @ConditionalOnMissingBean(PlatformTransactionManager.class)
    public PlatformTransactionManager transactionManager(){
        JpaTransactionManager manager = new JpaTransactionManager();
        manager.setEntityManagerFactory(entityManagerFactory());
        return manager;
    }

}

package com.technokratos.models;

import lombok.*;

@Data
@AllArgsConstructor
@Builder
@NoArgsConstructor
public class Student {

    private Long id;

    private String name;

    private Course course;
}

package com.technokratos.repository.impl;

import com.technokratos.repository.UserRepository;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

public class UserRepositoryJdbcImpl implements UserRepository {

    @Value("${db.url}")
    private String dbUrl;
}

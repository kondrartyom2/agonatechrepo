package com.technokratos.teststarter.service;

import com.technokratos.teststarter.model.User;

public interface UserService {
    User findById(Long id);

    void save(User user);
}

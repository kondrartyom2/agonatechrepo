package com.technokratos.model;

import lombok.EqualsAndHashCode;
import lombok.experimental.SuperBuilder;

import javax.persistence.*;
import java.util.List;

@SuperBuilder
@Entity
@Table(name = "course")
public class Course extends AbstractEntity {

    @Column(nullable = false)
    private Integer number;

    @Column(length = 50)
    private String direction;

    @OneToMany(fetch = FetchType.EAGER)
    @JoinColumn(name = "course_id")
    private List<Student> students;
}
